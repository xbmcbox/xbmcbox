#!/bin/sh
# Launch xbmc based on the opkg installation.
# -------------------------------------------

XBI=/opt/xbmc-bcm
XD=$XBI/xbmc-bin/bin
LD_LIBRARY_PATH=$XBI/xbmc-bin/lib
export XBMC_HOME=$XBI/xbmc-bin/share/xbmc

cd $XD
./xbmc --no-test

