# ---------------------------------------------------------------
# Build XBMC
# ---------------------------------------------------------------

# Include board specific targets
include configs/xbmc.mk.$(HW)

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

$(XBMC_T)-verify:
	@if [ "$(CROSS_COMPILER)" = "" ] || [ ! -f $(CROSS_COMPILER) ]; then \
		$(MSG11) "Can't find cross toolchain.  Try setting XI= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ "$(XBMC_ROOTFS)" = "" ] || [ ! -d $(XBMC_ROOTFS) ]; then \
		$(MSG11) "Can't find root filesystem.  Try setting ROOTFS= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ "$(XBMC_STAGING)" = "" ] || [ ! -d $(XBMC_STAGING) ] || [ ! -f $(XBMC_STAGING)/usr/bin/pkgconfig; then \
		$(MSG11) "Can't find buildroot staging tree.  Try setting STAGING= on the command line." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
.$(XBMC_T)-get $(XBMC_T)-get: 
	@mkdir -p $(BLDDIR) $(XBMC_ARCDIR)
	@if [ ! -d $(XBMC_ARCDIR)/$(XBMC_VERSION) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) "Retrieving XBMC source" $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(XBMC_ARCDIR) && git clone $(XBMC_URL) $(XBMC_VERSION); \
	else \
		$(MSG3) "XBMC source is cached" $(EMSG); \
	fi
	cd $(XBMC_ARCDIR)/$(XBMC_VERSION) && git checkout $(XBMC_GIT_ID)
	@touch .$(subst .,,$@)

$(XBMC_T)-get-plugin: 
	@if [ ! -f $(XBMC_ARCDIR)/$(XBMC_PLUGINS) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) "Retrieving XBMC plugins package" $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(XBMC_ARCDIR) && wget $(XBMC_PLUGINS_URL)/$(XBMC_PLUGINS); \
	else \
		$(MSG3) "XBMC plugins package is cached" $(EMSG); \
	fi

# Retrieve patches
.$(XBMC_T)-get-patch $(XBMC_T)-get-patch: .$(XBMC_T)-get
	@if [ ! -f $(XBMC_ARCDIR)/$(XBMC_PATCH_SCRIPT) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) "Retrieving XBMC patch script" $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(XBMC_ARCDIR) && wget $(XBMC_PATCH_URL)/$(XBMC_PATCH_SCRIPT); \
	else \
		$(MSG3) "XBMC patch script is cached" $(EMSG); \
	fi
	@touch .$(subst .,,$@)

# Unpack packages
.$(XBMC_T)-unpack $(XBMC_T)-unpack: .$(XBMC_T)-get-patch
	@$(MSG3) "Unpacking XBMC" $(EMSG)
	@mkdir -p $(XBMC_BLDDIR) $(XBMC_ARCDIR)
	@if [ -d $(XBMC_ARCDIR)/$(XBMC_VERSION) ]; then \
		cp -r $(XBMC_ARCDIR)/$(XBMC_VERSION) $(BLDDIR); \
	else \
		$(MSG11) "XBMC source archive is missing:" $(EMSG); \
		$(MSG11) "$(XBMC_ARCDIR)/$(XBMC_VERSION) not found " $(EMSG); \
		exit 1; \
	fi
	@cp $(XBMC_ARCDIR)/$(XBMC_PATCH_SCRIPT) $(XBMC_SRCDIR)
	@touch .$(subst .,,$@)

# Apply patches
.$(XBMC_T)-patch $(XBMC_T)-patch: .$(XBMC_T)-unpack
	@cd $(XBMC_SRCDIR) && sh patch_xbmc.sh
	@cd $(XBMC_SRCDIR) && rm patch_xbmc.sh
	@touch .$(subst .,,$@)

.$(XBMC_T)-init $(XBMC_T)-init: 
	@make $(XBMC_T)-verify
	@make .$(XBMC_T)-patch
	@touch .$(subst .,,$@)

# Build the package
$(XBMC_T): .$(XBMC_T)

.$(XBMC_T): .$(XBMC_T)-init 
	@$(MSG) "================================================================"
	@$(MSG2) "Building XBMC" $(EMSG)
	@$(MSG) "================================================================"
	@make $(XBMC_T)-config
	@cd $(XBMC_SRCDIR) && sh tools/rbp/setup-sdk.sh 
	@cd $(XBMC_SRCDIR) && make -C tools/rbp/depends/xbmc
	@cd $(XBMC_SRCDIR) && make -j2
	@cd $(XBMC_SRCDIR) && make install
	@touch .$(subst .,,$@)

# Package it as an opkg 
$(XBMC_T)-pkg: $(XBMC_T)-get-plugin opkg-verify
	@mkdir -p $(XBMC_BLDDIR)/opkg/xbmc/CONTROL
	@cp -ar $(XBMC_BLDDIR)/xbmc-bcm $(XBMC_BLDDIR)/opkg/xbmc/
	@cp -ar $(SCRIPTDIR)/xbmc.sh $(XBMC_BLDDIR)/opkg/xbmc/xbmc-bcm/xbmc-bin/bin
	@chmod +x $(XBMC_BLDDIR)/opkg/xbmc/xbmc-bcm/xbmc-bin/bin/xbmc.sh
	@sed -i 's%^prefix=.*%prefix=/opt/xbmc-bcm/xbmc-bin%' $(XBMC_BLDDIR)/opkg/xbmc/xbmc-bcm/xbmc-bin/bin/xbmc
	@tar -C $(XBMC_BLDDIR)/opkg/xbmc/xbmc-bcm/xbmc-bin/share/xbmc/addons/ -xvf $(XBMC_ARCDIR)/$(XBMC_PLUGINS) 
	@cp $(DIR_XBMC)/opkg/control $(XBMC_BLDDIR)/opkg/xbmc/CONTROL/control
	@cp $(DIR_XBMC)/opkg/postinst $(XBMC_BLDDIR)/opkg/xbmc/CONTROL/postinst
	@cp $(DIR_XBMC)/opkg/prerm $(XBMC_BLDDIR)/opkg/xbmc/CONTROL/prerm
	@chmod +x $(XBMC_BLDDIR)/opkg/xbmc/CONTROL/postinst
	@chmod +x $(XBMC_BLDDIR)/opkg/xbmc/CONTROL/prerm
	@cp $(DIR_XBMC)/opkg/debian-binary $(XBMC_BLDDIR)/opkg/xbmc/debian-binary
	@cd $(XBMC_BLDDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O xbmc
	@mkdir -p $(PKGDIR)/
	@cp $(XBMC_BLDDIR)/opkg/*.opk $(PKGDIR)/

$(XBMC_T)-pkg-clean: 
	@rm -rf $(XBMC_BLDDIR)/opkg

# Clean out the compiled package and packaging but leave the source tree.
$(XBMC_T)-clean:
	@cd $(XBMC_SRCDIR) && make clean
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm $(PKGDIR)/*; fi
	@if [ "$(XBMC_BLDDIR)" != "" ] && [ -d "$(XBMC_BLDDIR)" ]; then rm -rf $(XBMC_BLDDIR)/*; fi
	@rm -f $(XBMC_ARCDIR)/$(XBMC_PLUGINS)
	@rm -f .$(XBMC_T) 

# Clean out everything associated with XBMC
$(XBMC_T)-clobber: 
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm -rf $(PKGDIR); fi
	@if [ "$(XBMC_SRCDIR)" != "" ] && [ -d "$(XBMC_SRCDIR)" ]; then rm -rf $(XBMC_SRCDIR); fi
	@if [ "$(XBMC_BLDDIR)" != "" ] && [ -d "$(XBMC_BLDDIR)" ]; then rm -rf $(XBMC_BLDDIR); fi
	@rm -f $(XBMC_ARCDIR)/$(XBMC_PLUGINS)
	@rm -f .$(XBMC_T)-init .$(XBMC_T)-patch .$(XBMC_T)-unpack .$(XBMC_T)-get .$(XBMC_T)-get-patch
	@rm -f .$(XBMC_T) 

