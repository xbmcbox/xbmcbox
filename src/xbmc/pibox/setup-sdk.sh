#!/bin/bash

SCRIPT_PATH=$(cd `dirname $0` && pwd)
ROOTFS=[ROOTFS]
WORKDIR=[WORKDIR]
RPITC=[XCCDIR]
STAGING=[STAGING]

# Tell build that we're using buildroot
USE_BUILDROOT=1

BUILDROOT=$ROOTFS
SDKSTAGE=$STAGING
TARGETFS=$ROOTFS

TARBALLS=$WORKDIR/xbmc-tarballs
XBMCPREFIX=$WORKDIR/xbmc-bcm/xbmc-bin
TOOLCHAIN=$RPITC

mkdir -p $XBMCPREFIX
chmod 777 $XBMCPREFIX
mkdir -p $XBMCPREFIX/lib
mkdir -p $XBMCPREFIX/include

echo "SDKSTAGE=$SDKSTAGE"                                              >  $SCRIPT_PATH/Makefile.include
echo "XBMCPREFIX=$XBMCPREFIX"                                          >> $SCRIPT_PATH/Makefile.include
echo "TARGETFS=$TARGETFS"                                              >> $SCRIPT_PATH/Makefile.include
echo "TOOLCHAIN=$TOOLCHAIN"                                            >> $SCRIPT_PATH/Makefile.include
echo "BUILDROOT=$BUILDROOT"                                            >> $SCRIPT_PATH/Makefile.include
echo "USE_BUILDROOT=$USE_BUILDROOT"                                    >> $SCRIPT_PATH/Makefile.include
echo "BASE_URL=http://mirrors.xbmc.org/build-deps/darwin-libs"         >> $SCRIPT_PATH/Makefile.include
echo "TARBALLS_LOCATION=$TARBALLS"                                     >> $SCRIPT_PATH/Makefile.include
echo "RETRIEVE_TOOL=/usr/bin/curl"                                     >> $SCRIPT_PATH/Makefile.include
echo "RETRIEVE_TOOL_FLAGS=-Ls --create-dirs --output \$(TARBALLS_LOCATION)/\$(ARCHIVE)" >> $SCRIPT_PATH/Makefile.include
echo "ARCHIVE_TOOL=/bin/tar"                                           >> $SCRIPT_PATH/Makefile.include
echo "ARCHIVE_TOOL_FLAGS=xf"                                           >> $SCRIPT_PATH/Makefile.include
echo "JOBS=$((`grep -c processor /proc/cpuinfo -1`))"                  >> $SCRIPT_PATH/Makefile.include
