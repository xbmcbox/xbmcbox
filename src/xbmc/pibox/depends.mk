export HOST=arm-unknown-linux-gnueabi
export BUILD=i686-linux
export PREFIX=$(XBMCPREFIX)
export SYSROOT=$(TOOLCHAIN)/$(HOST)/sysroot
# 
export CFLAGS=-pipe -O3 -mcpu=arm1176jzf-s -mtune=arm1176jzf-s -mfloat-abi=hard -mfpu=vfp -mabi=aapcs-linux -Wno-psabi -Wa,-mno-warn-deprecated -Wno-deprecated-declarations
#
export CFLAGS+=-isystem$(TARGETFS)/opt/vc/include -isystem$(TARGETFS)/opt/vc/include/interface/vcos/pthreads -isystem$(TARGETFS)/usr/include/mysql -isystem$(TARGETFS)/usr/include -isystem$(TARGETFS)/usr/include/freetype2 -isystem$(TARGETFS)/usr/include/python2.7 -isystem$(TARGETFS)/usr/include/dbus-1.0 -isystem$(TARGETFS)/usr/lib/dbus-1.0/include -isystem$(SYSROOT)/usr/include -isystem$(PREFIX)/include
# 
export CXXFLAGS=$(CFLAGS) --sysroot=$(SYSROOT)
# export CPPFLAGS=$(CFLAGS) --sysroot=$(SYSROOT)
export LDFLAGS=-L$(SYSROOT)/lib -L$(SYSROOT)/usr/lib -L$(TARGETFS)/lib -L$(TARGETFS)/usr/lib -L$(TARGETFS)/opt/vc/lib -L$(XBMCPREFIX)/lib -L$(SDKSTAGE)/usr/$(HOST)/sysroot/lib -L$(SDKSTAGE)/usr/$(HOST)/sysroot/usr/lib 
#
export LD=$(TOOLCHAIN)/bin/$(HOST)-ld 
export CC=$(TOOLCHAIN)/bin/$(HOST)-gcc
export CXX=$(TOOLCHAIN)/bin/$(HOST)-g++
export OBJDUMP=$(TOOLCHAIN)/bin/$(HOST)-objdump
export RANLIB=$(TOOLCHAIN)/bin/$(HOST)-ranlib
export STRIP=$(TOOLCHAIN)/bin/$(HOST)-strip
export AR=$(TOOLCHAIN)/bin/$(HOST)-ar
export CXXCPP=$(CXX) -E
export PKG_CONFIG_PATH=$(TARGETFS)/usr/lib/pkgconfig
export PYTHON_VERSION=2.7
export PYTHON_NOVERSIONCHECK=1
# export PATH:=$(PREFIX)/bin:$(TOOLCHAIN)/bin:$(SYSROOT)/usr/bin:$(TARGETFS)/staging/usr/$(HOST)/sysroot/usr/bin:$(PATH)
export PATH:=$(PREFIX)/bin:$(TOOLCHAIN)/bin:$(SYSROOT)/usr/bin:$(PATH)
export TEXTUREPACKER_NATIVE_ROOT=/usr
# export PYTHON=$(SDKSTAGE)/usr/bin/python2.7
export PYTHON_LDFLAGS=-L$(TARGETFS)/usr/lib -lpython$(PYTHON_VERSION) -lpthread -ldl -lutil -lm -lz 
#
# export LIBS+=$(LDFLAGS) -ldbus-1 -lfribidi -lusb -ltag -lfreetype -lgpg-error
#
export FRIBIDI_CFLAGS=$(CFLAGS)
export FRIBIDI_LIBS=$(LDFLAGS) -lfribidi
#
export PNG_CFLAGS=$(CFLAGS)
export PNG_LIBS=$(LDFLAGS) -lpng
export FREETYPE2_CFLAGS=$(CFLAGS)
export FREETYPE2_LIBS=$(LDFLAGS) -lfreetype
export TAGLIB_CFLAGS=$(CFLAGS)
export TAGLIB_LIBS=$(LDFLAGS) -ltag
export DBUS_CFLAGS=$(CFLAGS)
export DBUS_LIBS=$(LDFLAGS) -ldbus-1
export LIBUSB_CFLAGS=$(CFLAGS)
export LIBUSB_LIBS=$(LDFLAGS) -lusb
export CEC_CFLAGS=$(CFLAGS)
export CEC_LIBS=$(LDFLAGS) -lcec
